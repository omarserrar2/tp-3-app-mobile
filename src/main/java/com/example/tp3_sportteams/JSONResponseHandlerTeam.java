package com.example.tp3_sportteams;

import android.util.JsonReader;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;


/**
 * Process the response to a GET request to the Web service
 * https://www.thesportsdb.com/api/v1/json/1/searchteams.php?t=R
 * Responses must be provided in JSON.
 *
 */


public class JSONResponseHandlerTeam {

    private static final String TAG = JSONResponseHandlerTeam.class.getSimpleName();

    private Team team;


    public JSONResponseHandlerTeam(Team team) {
        this.team = team;
    }

    /**
     * @param response done by the Web service
     * @return A Team with attributes filled with the collected information if response was
     * successfully analyzed
     */
    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readTeams(reader);
        } finally {
            reader.close();
        }
    }

    public void readTeams(JsonReader reader) throws IOException {

        reader.beginObject();
        while (reader.hasNext()) {
            String name;
            try {
                name = reader.nextName();
            }
            catch (IllegalStateException e){
                team.setIdTeam(-1);
                return;
            }

            if (name.equals("teams")) {
                readArrayTeams(reader);
            }
            else if(name.equals("table")){
                System.out.println("Lecture de la table: ");
                readArrayTable(reader);
            }
            else if(name.equals("results")){
                System.out.println("Lecture des resultats: ");
                readMatchArray(reader);
            }
            else {
                reader.skipValue();
            }
        }
        reader.endObject();
    }
    private void readMatchArray(JsonReader reader) throws IOException {
        reader.beginArray();
        int nb = 0; // only consider the first element of the array
        while (reader.hasNext() ) {
            Match match = new Match();
            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();
                if (nb==0){
                    System.out.println(name);
                    if (name.equals("strEvent")) {
                        match.setLabel(reader.nextString());
                      //  System.out.println(match.getLabel());
                    } else if (name.equals("strHomeTeam")) {
                        match.setHomeTeam(reader.nextString());
                     //   System.out.println(match.getHomeTeam());
                    } else if (name.equals("strAwayTeam")) {
                        match.setAwayTeam(reader.nextString());

                    } else if (name.equals("intHomeScore")) {
                        try{
                            int score = reader.nextInt();
                            match.setHomeScore(score);
                        }
                        catch(IllegalStateException e){
                            reader.nextNull();
                            match.setHomeScore(-1);
                        }
                    } else if (name.equals("intAwayScore")) {
                        try{
                            int score = reader.nextInt();
                            match.setAwayScore(score);
                        }
                        catch(IllegalStateException e){
                            reader.nextNull();
                            match.setAwayScore(-1);
                        }
                    } else {
                        reader.skipValue();
                    }
                }  else {
                    reader.skipValue();
                }
            }
            reader.endObject();
            if(nb==0){
                team.setLastEvent(match);
                System.out.println("Last Match "+team.getLastEvent().getHomeTeam());
            }

            nb++;
        }
        reader.endArray();
    }
    private void readArrayTable(JsonReader reader) throws IOException {
        reader.beginArray();
        int rank = 1;

        boolean trouvee = false;
        while (reader.hasNext() && !trouvee ) {
            reader.beginObject();
            while (reader.hasNext()) {

                String name = reader.nextName();
                if (name.equals("teamid")) {
                    long id = reader.nextLong();
                    System.out.println(id+" "+team.getIdTeam());
                    if(team.getIdTeam()==id){
                        team.setRanking(rank);
                        System.out.println("Classement de "+team.getName()+" : "+rank);
                        trouvee = true;
                    }

                }
                else if(trouvee && name.equals("total")){
                    team.setTotalPoints(reader.nextInt());
                }else {
                    reader.skipValue();
                }
            }
            rank++;
            if(!trouvee)reader.endObject();
         }

        if(!trouvee)reader.endArray();
    }

    private void readArrayTeams(JsonReader reader) throws IOException {
        try {
            reader.beginArray();
        }
        catch (IllegalStateException e){
            team.setIdTeam(-1);
            return;
        }
        int nb = 0; // only consider the first element of the array
        while (reader.hasNext() ) {
            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();
                if (nb==0) {
                    if (name.equals("idTeam")) {
                        try {
                            team.setIdTeam(reader.nextLong());
                        }
                        catch (IllegalStateException e){
                            team.setIdTeam(-1);
                            return;
                        }

                    } else if (name.equals("strTeam")) {
                        try {
                            team.setName(reader.nextString());
                        }
                        catch (IllegalStateException e){
                            team.setName("");
                            reader.nextNull();
                        }

                    } else if (name.equals("strLeague")) {
                        try {
                            team.setLeague(reader.nextString());
                        }
                        catch (IllegalStateException e){
                            team.setLeague("");
                            reader.nextNull();
                        }

                    } else if (name.equals("idLeague")) {
                        try {
                            team.setIdLeague(reader.nextLong());
                        }
                        catch (IllegalStateException e){
                            team.setIdLeague(-1);
                            reader.nextNull();
                        }

                    } else if (name.equals("strStadium")) {
                        try {
                            String stadium = reader.nextString();
                            System.out.println(stadium);
                            team.setStadium(stadium);
                        }
                        catch (IllegalStateException e){
                            team.setStadium("");
                            reader.nextNull();
                        }

                    } else if (name.equals("strStadiumLocation")) {
                        try {
                            team.setStadiumLocation(reader.nextString());
                        }
                        catch (IllegalStateException e){
                            team.setStadiumLocation("");
                            reader.nextNull();
                        }

                    } else if (name.equals("strTeamBadge")) {
                        try {
                            team.setTeamBadge(reader.nextString());
                        }
                        catch (IllegalStateException e){
                            team.setTeamBadge("");
                            reader.nextNull();
                        }

                    } else {
                        reader.skipValue();
                    }
                }  else {
                    reader.skipValue();
                }
            }
            reader.endObject();
            nb++;
        }
        reader.endArray();
    }

}
