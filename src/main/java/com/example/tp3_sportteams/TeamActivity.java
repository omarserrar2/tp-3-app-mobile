package com.example.tp3_sportteams;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.io.InputStream;


public class TeamActivity extends AppCompatActivity {

    private static final String TAG = TeamActivity.class.getSimpleName();
    private TextView textTeamName, textLeague, textManager, textStadium, textStadiumLocation, textTotalScore, textRanking, textLastMatch, textLastUpdate;


    private int totalPoints;
    private int ranking;
    private Match lastEvent;
    private String lastUpdate;

    private ImageView imageBadge;
    private Team team;
    private boolean hasChanged = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team);

        team = (Team) getIntent().getParcelableExtra(Team.TAG);
        if(team==null  ) System.out.println("Teaam is NULL");
        else System.out.println("Not null");
        textTeamName = (TextView) findViewById(R.id.nameTeam);
        textLeague = (TextView) findViewById(R.id.league);
        textStadium = (TextView) findViewById(R.id.editStadium);
        textStadiumLocation = (TextView) findViewById(R.id.editStadiumLocation);
        textTotalScore = (TextView) findViewById(R.id.editTotalScore);
        textRanking = (TextView) findViewById(R.id.editRanking);
        textLastMatch = (TextView) findViewById(R.id.editLastMatch);
        textLastUpdate = (TextView) findViewById(R.id.editLastUpdate);

        imageBadge = (ImageView) findViewById(R.id.imageView);
        hasChanged = false;
        updateView(false);

        final Button but = (Button) findViewById(R.id.button);

        but.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                TeamAPI.updateTeam(team, TeamActivity.this);

            }
        });

    }

    @Override
    public void onBackPressed() {
        if(hasChanged) {
            Intent intent = new Intent();
            intent.putExtra(Team.TAG, team);
            intent.putExtra(MainActivity.TYPE, MainActivity.SAVE);
            setResult(RESULT_OK, intent);
        }
        super.onBackPressed();
    }

    public void updateView(boolean updated) {
        if(team.getIdTeam()==-1 && updated){
            Snackbar.make(this.imageBadge.getRootView(), team.getName()+" n'existe pas !", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            hasChanged = false;
            return;
        }
        if(updated){
            hasChanged = true;
            Snackbar.make(this.imageBadge.getRootView(), " Retourner à la page principal pour enregitré !", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        }
        loadLogo();
        textTeamName.setText(team.getName());
        textLeague.setText(team.getLeague());
        textStadium.setText(team.getStadium());
        textStadiumLocation.setText(team.getStadiumLocation());
        textTotalScore.setText(Integer.toString(team.getTotalPoints()));
        textRanking.setText(Integer.toString(team.getRanking()));
        textLastMatch.setText(team.getLastEvent().toString());
        System.out.println("Last Match "+team.getLastEvent().getAwayTeam());
        textLastUpdate.setText(team.getLastUpdate());

	//TODO : update imageBadge
    }
    public void loadLogo(){
        String picUrl = team.getTeamBadge();
        if(picUrl == null || picUrl.equals("")) {
            imageBadge.setImageResource(R.drawable.default_logo);
        }
        else {
            new DownloadImageTask(imageBadge)
                    .execute(picUrl);
        }
    }


}
class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
    ImageView badgeImageView;

    public DownloadImageTask(ImageView badgeImageView) {
        this.badgeImageView = badgeImageView;
    }

    protected Bitmap doInBackground(String... url) {
        Bitmap badgeBitmap = null;
        String badgeUrl = url[0];

        try {
            InputStream inputStream = new java.net.URL(badgeUrl).openStream();
            badgeBitmap = BitmapFactory.decodeStream(inputStream);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return badgeBitmap;
    }

    protected void onPostExecute(Bitmap result) {
        badgeImageView.setImageBitmap(result);
    }
}
