package com.example.tp3_sportteams;

import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

public class RowHolder extends RecyclerView.ViewHolder
        implements View.OnClickListener {
    TextView nomEquipeTextView =null;
    TextView leagueTextView =null;
    ImageView logo = null;
    Team team;
    RowHolder(View row) {
        super(row);

        nomEquipeTextView=(TextView)row.findViewById(R.id.name_text);
        leagueTextView=(TextView)row.findViewById(R.id.league_text);
        logo = (ImageView)row.findViewById(R.id.logo);
        row.setOnClickListener(this);
    }

    @Override
    public void onClick(View v){
        Intent myIntent = new Intent(v.getContext(), TeamActivity.class);
        myIntent.putExtra(Team.TAG, team);
        ((MainActivity)v.getContext()).startActivityForResult(myIntent, 1);
        //;
    }

    void bindModel(Team team) {
        if(team!=null){
           this.team = team;
            nomEquipeTextView.setText(team.getName());
            leagueTextView.setText(team.getLeague());
            String teamBadge = team.getTeamBadge();
            logo.setImageDrawable(null);
            if(teamBadge!=null && !teamBadge.equals(""))
                new DownloadImageTask(logo).execute(teamBadge);
            else
                logo.setImageResource(R.drawable.default_logo);
        }
    }
}
