package com.example.tp3_sportteams;

import android.os.AsyncTask;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class TeamAPI  extends AsyncTask<Team, String, Team[]> {
    AppCompatActivity activity = null;
    TeamAPI(){
    }
    TeamAPI(AppCompatActivity activity){
        this.activity = activity;
    }
    public static void updateTeam(Team team, AppCompatActivity activity){

        new TeamAPI(activity).execute(team);

    }
    public static void updateAll( AppCompatActivity activity, Team... teams){
        new TeamAPI(activity).execute(teams);
    }
    private void APICall(Team team, URL url) throws IOException {
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        InputStream in = new BufferedInputStream(urlConnection.getInputStream());
        JSONResponseHandlerTeam jsonResponseHandlerTeam = new JSONResponseHandlerTeam(team);
        jsonResponseHandlerTeam.readJsonStream(in);
    }
    @Override
    protected Team[] doInBackground(Team... teams) {
        for(Team team: teams){
            try {
                URL nameURL = WebServiceUrl.buildSearchTeam(team.getName());
                System.out.println(nameURL);
                APICall(team, nameURL);
                if(team.getIdTeam()==-1){
                    continue;
                }
                URL rankURL = WebServiceUrl.buildGetRanking(team.getIdLeague());
                System.out.println(rankURL);
                APICall(team, rankURL);
                URL lastEventsURL = WebServiceUrl.buildSearchLastEvents(team.getIdTeam());
                System.out.println(lastEventsURL);
                APICall(team, lastEventsURL);

                System.out.println("Success");

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return teams;
    }
    @Override
    protected void onPostExecute(Team[] teams) {
        if(activity!=null && activity instanceof TeamActivity) ((TeamActivity) activity).updateView(true);
        else if(activity instanceof  MainActivity){
            ((MainActivity)activity).endRefresh(teams);
        }
    }


}
