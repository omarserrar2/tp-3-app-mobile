package com.example.tp3_sportteams;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerViewAccessibilityDelegate;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private RecyclerView teamsList;
    private SportDbHelper sportDbHelper;
    public static final int SAVE = 0, ADD = 1;
    public static final String TYPE = "TYPE";
    private SwipeRefreshLayout refreshLayout;
    private Cursor teamCursor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        refreshLayout = findViewById(R.id.refresh);
        refreshLayout.setOnRefreshListener(this);
        setSupportActionBar(toolbar);
        sportDbHelper = new SportDbHelper(this);

        actualiserLaListe();
        if(sportDbHelper.justCreated){
            sportDbHelper.populate();
            actualiserLaListe();
        }
        FloatingActionButton fab = findViewById(R.id.fab);
       /* teamsList.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            public void onItemClick(AdapterView<?> parent, View view, int position, long id){

                Intent myIntent = new Intent(MainActivity.this, TeamActivity.class);

                Cursor cursor = (Cursor) parent.getItemAtPosition(position);
                Team team = sportDbHelper.cursorToTeam(cursor);
                if(team==null  ) System.out.println("Teaam is NULL");
                else System.out.println("Not null");
                myIntent.putExtra(Team.TAG, team);
                startActivityForResult(myIntent, 1);
            }
        });*/

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(MainActivity.this, NewTeamActivity.class);
                startActivityForResult(myIntent, 1);
            }
        });
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK) {
            int type = data.getIntExtra(TYPE, -1);
            if(type==ADD){
                Team team = data.getParcelableExtra(Team.TAG);
                boolean teamAdded = sportDbHelper.addTeam(team);
                if(teamAdded){
                    Snackbar.make(teamsList.getRootView(), team.getName()+" Ajouté !", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    actualiserLaListe();

                }
                else{
                    Snackbar.make(teamsList.getRootView(), " Erreurs !", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    actualiserLaListe();
                }
            }
            else if (type==SAVE){
                Team team = data.getParcelableExtra(Team.TAG);
                boolean teamUpdated = sportDbHelper.updateTeam(team)>0;
                if(teamUpdated){
                    Snackbar.make(teamsList.getRootView(), team.getName()+" Enregistré !", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    actualiserLaListe();

                }
            }
            else{
                Snackbar.make(teamsList.getRootView(), type+" Problem ! "+ data.getExtras().toString(), Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        }
    }
    public void actualiserLaListe(){
        teamCursor = sportDbHelper.fetchAllTeams();
        teamsList = findViewById(R.id.teams_list);
        teamsList.setLayoutManager(new LinearLayoutManager(this));
        teamsList.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        teamsList.setAdapter(new TeamListAdapter());
        new ItemTouchHelper(deleteOnSwipe).attachToRecyclerView(teamsList);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {
        if(!teamCursor.moveToFirst()){
            endRefresh(null);
            return;
        }
        System.out.println("Here");
        ArrayList<Team> teams = new ArrayList<Team>();
        teams.add(sportDbHelper.cursorToTeam(teamCursor));
        while(teamCursor.moveToNext()){
           Team team = sportDbHelper.cursorToTeam(teamCursor);
            teams.add(team);
        }
        TeamAPI.updateAll(MainActivity.this, teams.toArray(new Team[teams.size()]));
    }
    public void endRefresh(Team[] teams){
        int i=0;
        if(teams!=null)
        for(Team team: teams){
            if(team.getIdTeam()==-1){
                continue;
            }

            boolean teamUpdated = sportDbHelper.updateTeam(team)>0;
            if(teamUpdated) i++;
        }
        actualiserLaListe();
        Snackbar.make(teamsList.getRootView(), i+" Team actualisées", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
        refreshLayout.setRefreshing(false);
    }

    class TeamListAdapter extends RecyclerView.Adapter<RowHolder> {
        @Override
        public RowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return (new RowHolder(getLayoutInflater().inflate(R.layout.team_row, parent, false)));
        }

        @Override
        public void onBindViewHolder(RowHolder holder, int position) {
            try {
                teamCursor.moveToPosition(position);
                holder.bindModel(sportDbHelper.cursorToTeam(teamCursor));
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return teamCursor.getCount();
        }
    }

    ItemTouchHelper.SimpleCallback deleteOnSwipe = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT| ItemTouchHelper.RIGHT) {
        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
            teamCursor.moveToPosition(viewHolder.getAdapterPosition());
            final Team team = sportDbHelper.cursorToTeam(teamCursor);
            final int adapterPos = viewHolder.getAdapterPosition();
            new AlertDialog.Builder(MainActivity.this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle(team.getName())
                    .setMessage("Voullez vous vraiment supprimer cette équipe ?")
                    .setPositiveButton("Oui", new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {


                            sportDbHelper.deleteTeam(team.getId());
                            actualiserLaListe();
                            Snackbar.make(teamsList.getRootView(), team.getName()+" a été supprimé ! ", Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                        }

                    })
                    .setNegativeButton("Non", new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            teamsList.getAdapter().notifyItemChanged(adapterPos);
                        }

                    } )
                    .show();

        }
    };


}
